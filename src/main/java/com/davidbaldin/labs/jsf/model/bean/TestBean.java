package com.davidbaldin.labs.jsf.model.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ManagedBean
@RequestScoped
public class TestBean {

	static final Logger LOG = LoggerFactory.getLogger(TestBean.class);

	public TestBean() {
		LOG.debug("Constructed " + this.getClass().getSimpleName());
	}

	public String getBlub() {
		return "Blub";
	}
}